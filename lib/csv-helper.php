<?php

class CSVHelper{
	public static function _arrayToJSON($input=array(),$output=''){
		if(!is_array($input)){
			return false;
		}
		if($output != ''){
			$fp = fopen($output, 'w');
			fwrite($fp, json_encode($input,JSON_UNESCAPED_UNICODE));
			fclose($fp);
		}
	}
	public static function _listDir($path){
		$thelist = array();
		if ($handle = opendir($path)) {
			while (false !== ($file = readdir($handle)))
			{
				if ($file != "." && $file != ".." && strtolower(substr($file, strrpos($file, '.') + 1)) == 'pdf')
				{
					$thelist[] = $file;
				}
			}
			closedir($handle);
		}
		return $thelist;
	}
	public static function _parseJSONFile($input=''){
		if($input!=''){
			$str = file_get_contents($input);
			$json = json_decode($str, true); // decode the JSON into an associative array
			return $json;
		}
	}
	public static function _csvToArray($filename=''){
		//credits to: http://steindom.com/articles/shortest-php-code-convert-csv-associative-array
		$rows = array_map('str_getcsv', file($filename));
		$header = array_shift($rows);
		$csv = array();
		foreach ($rows as $row) {
			$csv[] = array_combine($header, $row);
		}
		return $csv;
	}
}
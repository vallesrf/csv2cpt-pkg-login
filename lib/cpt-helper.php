<?php

class CPTHelper{
	
	public static function register($singularName='',$pluralName='',$supports=array(),$description=''){
		
		$labels = array(
				'name'               => _x( $pluralName, 'post type general name' ),
				'singular_name'      => _x( $singularName, 'post type singular name' ),
				'add_new'            => _x( 'Add New', strtolower($singularName) ),
				'add_new_item'       => __( 'Add New '.$singularName ),
				'edit_item'          => __( 'Edit '.$singularName ),
				'new_item'           => __( 'New '.$singularName ),
				'all_items'          => __( 'All '.$pluralName ),
				'view_item'          => __( 'View '.$pluralName ),
				'search_items'       => __( 'Search '.$pluralName ),
				'not_found'          => __( 'No '.strtolower($pluralName).' found' ),
				'not_found_in_trash' => __( 'No '.strtolower($pluralName).' found in the Trash' ),
				'menu_name'          => $pluralName
		);
		$args = array(
				'labels'        => $labels,
				'description'   => $description,
				'public'        => true,
				'menu_position' => 5, 
				'has_archive'   => true,
				'supports' => $supports
		);
		if ( ! post_type_exists( $singularName ) ) {
			register_post_type( $singularName, $args );
    }
  }
	public static function registerTaxonomy($cpt='',$singularName='',$pluralName=''){
		$labels = array(
				'name'              => _x( $cpt.' '.$pluralName, 'taxonomy general name' ),
				'singular_name'     => _x( $cpt.' '.$singularName, 'taxonomy singular name' ),
				'search_items'      => __( 'Search '.$cpt.' '.$pluralName ),
				'all_items'         => __( 'All '.$cpt.' '.$pluralName ),
				'parent_item'       => __( 'Parent '.$cpt.' '.$singularName ),
				'parent_item_colon' => __( 'Parent '.$cpt.' '.$singularName .':' ),
				'edit_item'         => __( 'Edit '.$cpt.' '.$singularName  ),
				'update_item'       => __( 'Update '.$cpt.' '.$singularName  ),
				'add_new_item'      => __( 'Add New '.$cpt.' '.$singularName  ),
				'new_item_name'     => __( 'New '.$cpt.' '.$singularName  ),
				'menu_name'         => __( $cpt.' '.$pluralName  ),
		);
		$args = array(
				'labels' => $labels,
				'hierarchical' => true,
		);
		if ( ! taxonomy_exists( strtolower($cpt).'_'.strtolower($singularName) ) ) {
			register_taxonomy( strtolower($cpt).'_'.strtolower($singularName), strtolower($cpt), $args );
		}
		
	}
	public static function wpGetPosts($args=array()){
		$results = get_posts( $args );
		return $results;
	}
	public static function wpDeletePost($postID='',$forceDelete=false){
		if($postID){
			wp_delete_post($postID,$forceDelete);
		}
	}
	public static function createPost($title='',$post_type='post',$post_status='publish'){
		/**
		 * A function used to programmatically create a post in WordPress. The slug and title
		 * are defined within the context of the function.
		 *
		 * @returns -1 if the post was never created
		 */
		
		// Initialize the page ID to -1. This indicates no action has been taken.
		$post_id = -1;
		
		// Set the post ID so that we know the post was created successfully
		
			$post_id = wp_insert_post(
					array(
							'comment_status'	=>	'closed',
							'ping_status'		=>	'closed',
							'post_title'		=>	$title,
							'post_status'		=>	$post_status,
							'post_type'		=>	$post_type
					)
			);
			
		
		
		return $post_id;
	}
}

<?php
/*
 * Plugin Name: CSV2CPT-BENCOR-LOGIN
 * Version: 1.3
 * Description: The CSV2CPT Wordpress Plugin helps users transform CSV data into Wordpress Custom Posts.
 * Author: OBLIVIO LLC
 * Author URI: https://www.obliviocompany.com/
 * require_onces at least: 4.0
 * Tested up to: 4.0
 *
 * Text Domain: wp-csv2cpt
 *
 * @package WordPress
 * @author OBLIVIO LLC
 * @since 1.0.0
 */
ini_set('display_errors',1);
if ( ! defined( 'ABSPATH' ) ) exit;

function cleanCSVinput($str){
	$res = addslashes($str);//str_replace(",","__comma__",$str);
	return $res;
}
function uncleanCSVinput($str){
	$res = stripslashes($str);//str_replace("__comma__",",",$str);
	return $res;
}

function csv2cpt_init() {
	//CSVHelper
	require_once __DIR__ . '/lib/csv-helper.php';
	//CPTHelper
	require_once __DIR__ . '/lib/cpt-helper.php';
	//Configuration
	require_once __DIR__ . '/config/config-all.php';

	
	$csvdata = CSVHelper::_csvToArray(__DIR__.'/data/data.csv');
	
	
	//Register CPT
	CPTHelper::register($config['CPT']['SingularName'],$config['CPT']['PluralName'],$config['CPT']['CPTSupports'],$config['CPT']['Description']);
	
	
	//Register Taxonomies
	foreach(array_keys($csvdata[0]) as $i=>$columnName){
		foreach($config['CPT']['Taxonomies'] as $j=>$taxonomy){
			if($columnName == $taxonomy['CSV_Column']){
				CPTHelper::registerTaxonomy($config['CPT']['SingularName'],$taxonomy['Singular'],$taxonomy['Plural']);
			}
		}
	} //taxonomies have been registered
    
}
add_action('init', 'csv2cpt_init');

add_action('admin_menu', 'csv2cpt_setup_menu');
 
function csv2cpt_setup_menu(){
        add_menu_page( 'CPT Import', 'CPT Import', 'manage_options', 'csv2cpt-plugin-import', 'csv2cpt_setup_menu_init' );
}
 
function csv2cpt_setup_menu_init(){
        echo "<h1>CPT Import</h1><hr /><input type='text' value='' id='csv2cpt-interval' placeholder='Import Interval' /><button type='button' id='csv2cpt-import-btn'>IMPORT</button><hr /><button id='csv2cpt-delete-btn' type='button'>Delete Previously Imported</button>";
}

add_action( 'wp_ajax_csv2cpt_import', 'csv2cpt_import' );
add_action( 'wp_ajax_csv2cpt_delete', 'csv2cpt_delete');

function csv2cpt_delete(){
    //CPTHelper
	require_once __DIR__ . '/lib/cpt-helper.php';
    //Configuration
	require __DIR__ . '/config/config-all.php';

    //delete previous imports
	$delQueryArgs = array(
			'orderby' => 'ID',
			'numberposts'=>'-1', //returns all
			'post_status' => 'publish',
			'post_type' => $config['CPT']['SingularName'],
			'meta_query' => array(
					array(
							'key' => 'csv2cpt_import',
							'value' => "Y"
					),
			)
	);
	$oldimports = CPTHelper::wpGetPosts($delQueryArgs);

	if(count($oldimports) > 0){
		foreach($oldimports as $i=>$oldimport){
			 CPTHelper::wpDeletePost($oldimport->ID,true);
		}	
	}
	echo json_encode(array('msg'=>'deleted:'.count($oldimports),'err'=>false));
	//end delete
    wp_die(); // this is required to terminate immediately and return a proper response

}
function csv2cpt_import() {
	global $wpdb; // this is how you get access to the database
    require_once __DIR__ . '/lib/csv-helper.php';
	//CPTHelper
	require_once __DIR__ . '/lib/cpt-helper.php';
	//Configuration
	require __DIR__ . '/config/config-all.php';

    $csvdata = CSVHelper::_csvToArray(__DIR__.'/data/data.csv');
	
    
    if(isset($_POST['csv2cpt_index'])){
        $tmpIndex = (int) $_POST['csv2cpt_index'];
        $increment = (int) $_POST['csv2cpt_increment'];
        $tmpLimit = $tmpIndex + $increment;

        if($tmpLimit >= count($csvdata)){
            $tmpLimit = count($csvdata);
        }
        
        for($i = $tmpIndex; $i < $tmpLimit; $i++){
            $csvRow = $csvdata[$i];
            $tmpPostTitle = $csvRow['advisor name'];
            $metaData = array();
            foreach($config['CPT']['CustomMeta'] as $k=>$metaObj){
                $metaData[$metaObj['meta_key']] = cleanCSVinput($csvRow[$metaObj['CSV_Column']]);
            }
            $metaData['csv2cpt_import'] = "Y";
            $taxonomyData=array();
            foreach($config['CPT']['Taxonomies'] as $k=>$metaObj){
                $taxonomyData[$metaObj['tax_key']] = cleanCSVinput($csvRow[$metaObj['CSV_Column']]);
            }
            $tmpPostID = CPTHelper::createPost($tmpPostTitle,$config['CPT']['SingularName']);

            if($tmpPostID !== -1){
                foreach($taxonomyData as $tax_name=>$tax_val){
                    wp_set_object_terms( $tmpPostID, $tax_val, $tax_name);
                }
                foreach($metaData as $meta_key=>$meta_val){
                    add_post_meta($tmpPostID, $meta_key, $meta_val);
                }
            }
        }
        $response = array();
        $response['next_start'] = $tmpLimit;
        echo json_encode($response);
    
    }
	wp_die(); // this is required to terminate immediately and return a proper response
}

add_action( 'admin_footer', 'csv2cpt_javascript' ); // Write our JS below here

function csv2cpt_javascript() { ?>
	<script type="text/javascript" >
	jQuery(document).ready(function($) {
        window.csv2cpt_cache = {};
        window.csv2cpt_cache['current_index'] = 0;
        
        $('#csv2cpt-import-btn').off('click').on('click',function(e){
            var realIncrement = parseInt($("#csv2cpt-interval").val(),10);
            $("#csv2cpt-interval").attr('disabled',true);
            var data = {
                'action': 'csv2cpt_import',
                'csv2cpt_index': window.csv2cpt_cache['current_index'],
                'csv2cpt_increment': realIncrement
            };
            $('#csv2cpt-import-btn')[0].innerHTML = 'Please wait...';   
            jQuery.post(ajaxurl, data, function(response) {
                var response = JSON.parse(response);
                console.log('response',response);
                if(window.csv2cpt_cache['current_index'] == response['next_start']){
                    
                    $('#csv2cpt-import-btn')[0].innerHTML = 'Done Importing';
                    $('#csv2cpt-import-btn').attr('disabled',true);
                }else{
                    
                    $('#csv2cpt-import-btn')[0].innerHTML = 'Next Batch';   
                }
                
                window.csv2cpt_cache['current_index'] = response['next_start'];
            });
        });
        $('#csv2cpt-delete-btn').off('click').on('click',function(e){
            var data = {
                'action': 'csv2cpt_delete',
            };
            jQuery.post(ajaxurl, data, function(response) {
                var response = JSON.parse(response);
                alert(response['msg']);
                location.reload(true);
            });
        });
        
            
	});
	</script> <?php
}






add_action('add_meta_boxes', 'benc_setup_advisor_fields');
add_action('save_post', 'benc_advisor_save_events_meta', 1, 2);

function benc_setup_advisor_fields() {
  add_meta_box('benc_advisor_information', 'Advisor Information', 'benc_advisor_fields', 'Advisor', 'normal', 'high');
  wp_nonce_field( plugin_basename(__FILE__), 'bencoradvisors_noncename' );
}

function benc_advisor_fields() {
  global $post;
  echo '<input type="hidden" name="bencoradvisors_noncename" id="bencoradvisors_noncename" value="' . wp_create_nonce(plugin_basename(__FILE__)). '" />';
  $advisor_meta['advisor_address_1'] = get_post_meta($post->ID, 'advisor_address_1', true);
  $advisor_meta['advisor_address_2'] = get_post_meta($post->ID, 'advisor_address_2', true);
  $advisor_meta['advisor_office_city'] = get_post_meta($post->ID, 'advisor_office_city', true);
  $advisor_meta['advisor_office_zip']  = get_post_meta($post->ID, 'advisor_office_zip', true);
  $advisor_meta['email'] = get_post_meta($post->ID, 'email', true);
  $advisor_meta['phone_cell'] = get_post_meta($post->ID, 'phone_cell', true);
  $advisor_meta['phone_fax'] = get_post_meta($post->ID, 'phone_fax', true);
  $advisor_meta['phone_office'] = get_post_meta($post->ID, 'phone_office', true);

  foreach ($advisor_meta as $key => $value) {
    echo '<div style="margin-bottom: 1rem;">'; 
    echo '<label><strong>' . ucwords(str_replace('_', ' ', $key)) . '</strong>';
    echo '<input type="text" name="' . $key . '" value="' . $value . '" class="widefat" />';
    echo '</label></div>';
  }
}

function benc_advisor_save_events_meta($post_id, $post) {
  if ( !isset($_POST['bencoradvisors_noncename']) || !wp_verify_nonce($_POST['bencoradvisors_noncename'], plugin_basename(__FILE__))) {
    return $post->ID;
  }
  if (!current_user_can('edit_post', $post->ID)) {
    return $post->ID;
  }
  $advisor_meta['advisor_name'] = $post->post_title;
  $advisor_meta['advisor_address_1'] = $_POST['advisor_address_1'];
  $advisor_meta['advisor_address_2'] = $_POST['advisor_address_2'];
  $advisor_meta['advisor_office_city'] = $_POST['advisor_office_city'];
  $advisor_meta['advisor_office_zip']  = $_POST['advisor_office_zip'];
  $advisor_meta['email'] = $_POST['email'];
  $advisor_meta['phone_cell'] = $_POST['phone_cell'];
  $advisor_meta['phone_fax'] = $_POST['phone_fax'];
  $advisor_meta['phone_office'] = $_POST['phone_office'];

  foreach ($advisor_meta as $key => $value) {
    if ($post->post_type == 'revision' ) { 
      return;
    }

    $value = implode(',', (array)$value);

    if (get_post_meta($post->ID, $key, FALSE)) {
      update_post_meta($post->ID, $key, $value);
    } else {
      add_post_meta($post->ID, $key, $value);
    }

    if (!$value) { 
      delete_post_meta($post->ID, $key);
    }
  }
}

function advisor_columns($columns){
	$columns = array(
			"cb" => "<input type=\"checkbox\" />",
			"title" => "Name",
// 			"advisor_name"=> "Name",
			"advisor_state"=> "State",
			"advisor_city"=> "City",
			"advisor_county"=> "County",
			"advisor_employer"=> "Employer",
            "advisor_portal"=>"Portal",
            "advisor_login"=>"Login",
	);

	return $columns;
}
function show_advisor_columns($column){
	global $post;

	switch ($column) {
		case 'email':
		case 'advisor_name':
		case 'phone_office':
		case 'phone_fax':
		case 'phone_cell':
		case 'advisor_address_1':
		case 'advisor_address_2':
		case 'advisor_office_zip':
		case 'advisor_office_city':
			$meta = get_post_meta($post->ID,$column);
			echo uncleanCSVinput($meta[0]);
			break;
        case 'advisor_portal':
        case 'advisor_login':
            $tax = wp_get_post_terms($post->ID,$column);
			echo uncleanCSVinput((string)$tax[0]->name);
            break;
		case 'advisor_state':
		case 'advisor_city':
		case 'advisor_county':
		case 'advisor_employer':
			$tax = wp_get_post_terms($post->ID,$column);
			echo uncleanCSVinput($tax[0]->name);
			break;
		default:
			echo $post->ID.":".$column;
	}
}
add_action("manage_posts_custom_column",  "show_advisor_columns");
add_filter("manage_edit-advisor_columns", "advisor_columns");


<?php
return array(
	'CPTName'=>'Advisors',
    'BATCH_LIMIT'=>50,
	'CPTSupports'=>array( 
			'title',
            'revisions',
            'page-attributes'
		),
	'SingularName'=>'Advisor',
	'PluralName'=>'Advisors',
	'Description'=>'Advisors',
	'Taxonomies'=>array(
				array('tax_key'=>'advisor_city','Singular'=>'City','Plural'=>'Cities','CSV_Column'=>'Employer City'),
				array('tax_key'=>'advisor_county','Singular'=>'County','Plural'=>'Counties','CSV_Column'=>'Employer County'),
				array('tax_key'=>'advisor_state','Singular'=>'State','Plural'=>'States','CSV_Column'=>'state'),
				array('tax_key'=>'advisor_employer','Singular'=>'Employer','Plural'=>'Employers','CSV_Column'=>'employer'),
				array('tax_key'=>'advisor_portal','Singular'=>'Portal','Plural'=>'Portals','CSV_Column'=>'Portal Website'),
				array('tax_key'=>'advisor_login','Singular'=>'Login','Plural'=>'Logins','CSV_Column'=>'Employer Portal Website'),				
		),
	'CustomMeta'=>array(
			array('meta_key'=>'advisor_name','CSV_Column'=>'advisor name'),
			array('meta_key'=>'email','CSV_Column'=>'email'),
			array('meta_key'=>'phone_fax','CSV_Column'=>'phone_fax'),
			array('meta_key'=>'phone_cell','CSV_Column'=>'phone_cell'),
			array('meta_key'=>'phone_office','CSV_Column'=>'phone_office'),
			array('meta_key'=>'advisor_office_city','CSV_Column'=>'Advisor Ofc City'),
			array('meta_key'=>'advisor_office_zip','CSV_Column'=>'Advisor Office zip'),
			array('meta_key'=>'advisor_address_1','CSV_Column'=>'address_1'),
			array('meta_key'=>'advisor_address_2','CSV_Column'=>'address_2'),
	),
);

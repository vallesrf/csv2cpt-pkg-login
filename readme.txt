=== WP-CSV2CPT Plugin Template ===
Contributors: obliviollc
Tags: wordpress, plugin, csv, cpt
Requires at least: 3.9
Tested up to: 4.0
Stable tag: 1.0
License: GPLv3 
License URI: http://www.gnu.org/licenses/gpl-3.0.html

The CSV2CPT Wordpress Plugin developed by OBLIVIO LLC.

== Description ==

The CSV2CPT Wordpress Plugin helps users transform CSV data into Wordpress Custom Posts.


== Installation ==

Installing "WordPress CSV2CPT Plugin" can be done either by using the following steps:

1. Download the plugin package (.zip)
2. Upload the ZIP file through the 'Plugins > Add New > Upload' screen in your WordPress dashboard
3. Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==

= What is the plugin used for? =

This plugin is designed to help you import data from a .CSV into Custom Wordpress Post Types using Taxonomies and Meta Data.

== Changelog ==

= 1.0 =
* 2016-06-11
* Initial release


